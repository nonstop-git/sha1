# LICENSE #

Sha1tool for NonStop License

### Terms and Conditions ###

Sha1tool is released under the [GNU General Public License version 2.0](http://opensource.org/licenses/GPL-2.0),
which is an [open source license](http://www.opensource.org/docs/osd). The Sha1tool project chose to use GPLv2
to guarantee your freedom to share and change free software---to make sure the software is free for all its users.

### Who do I talk to? ###

* Talk to the repository owner.
* Randall Becker (randall.becker@nexbridge.ca)