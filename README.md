# README #

Sha1 Tool for NonStop README

### What is this repository for? ###

* Wrapper for the sha1.c open source code variant
* Version 1.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Clone this repository
* No configuration is required
* Depends on the ns-license submodule
* This code packages both an OSS and GUARDIAN package for L and J series

### Contribution guidelines ###

* None

### Who do I talk to? ###

* Talk to the repository owner.
* Randall Becker (randall.becker@nexbridge.ca)