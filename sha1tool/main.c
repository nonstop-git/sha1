/**
 * @file
 * Main loop.
 * @author Randall S. Becker
 * @copyright Copyright (c) 2017 Nexbridge Inc. This code is supplied under the
 * <a href="http://opensource.org/licenses/GPL-2.0">GNUv2 Public License version 2.</a>
 */
#define _LARGEFILE64_SOURCE 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wchar.h>

#include "sha1.h"
#include "version.h"

/**
 * The indicator of whether to keep as quiet as possible in reporting.
 */
static bool quiet = false;
/**
 * The supplied hash value. There may not be one, in which case hashes are just reported
 * instead of validated.
 */
static const char *hash = NULL;

/**
 * Dump the online help for this product.
 * @param progname
 *     the program name, typically from argv[0].
 */
void help(const char *progname) {
	wprintf(
			L"usage: %s ( --version | --usage | --help | --copyright | file ... )\n",
			progname);
	wprintf(
			L"usage: %s ( --quiet | -q | --file file-name | --hash=sha1-hash-value )\n",
			progname);
	wprintf(
			L"The first form generates a set of hash values. If --quiet is used, only\n"
					L"the hash values are output.\n"
					L"The second form is used to test a single file for a matching hash.\n"
					L"The completion code will indicate whether there was a match.\n");
}

/**
 * The Usage procedure. This delegates to {@link help} and exits.
 * @param progname
 *     the program name, typically from argv[0].
 */
void Usage(const char *progname) {
	help(progname);
	exit(1);
}

/**
 * Run the validation. This opens the supplied file, reads the file into a dynamically allocated buffer,
 * computes the hash, and either reports the hash or compares it to a previously supplied {@link #hash}.
 * @param filename
 *     the file name to load and on which to compute a SHA1 hash.
 * @returns true if the hash validates or if the hash was just output without comparison.
 */
bool validate(const char *filename) {
	blk_SHA_CTX context;
	struct stat statbuf;
	unsigned char hashout[20];
	char output[64];

	if (!stat(filename, &statbuf)) {
		char *buf = malloc((size_t) (statbuf.st_size * 1.5));
		FILE *file;
		size_t reading = 0;
		if (!buf) {
			fprintf(stderr,
					"%s: too big (%d) to allocate memory to compute hash\n",
					filename, statbuf.st_size);
			return false;
		}
		file = fopen_oss(filename, "rb");
		if (!file) {
			file = fopen_guardian(filename, "rb");
			if (!file) {
				perror(filename);
				return false;
			}
		}

		while (reading < statbuf.st_size) {
			size_t readCount = fread(buf + reading, 1, statbuf.st_size, file);
			if (readCount == 0)
				break;
			reading += readCount;
		}

		fclose(file);

		blk_SHA1_Init(&context);
		blk_SHA1_Update(&context, buf, reading);
		blk_SHA1_Final(hashout, &context);
		blk_SHA1_ToAscii(hashout, output);

		if (quiet) {
			if (hash) {
				return strcmp(hash, output) == 0;
			}
			wprintf(L"%s\n", output);
			return true;
		} else {
			if (hash) {
				if (strcmp(hash, output) == 0) {
					wprintf(L"Matched\n");
					return true;
				} else {
					wprintf(L"Hash mismatched\n");
					return false;
				}
			} else {
				wprintf(L"%s: %s\n", filename, output);
				return true;
			}
		}
	} else {
		perror(filename);
		return false;
	}
}

int main(int argc, char **argv, char **envp) {
	bool accumulated = true;

	for (size_t index = 1; index < argc; index++) {
		if (strcmp(argv[index], "--version") == 0) {
			if (index != 1)
				Usage(argv[0]);
			version(argc, argv);
			return 0;
		}
		if (strcmp(argv[index], "--usage") == 0
				|| strcmp(argv[index], "--help") == 0) {
			if (index != 1)
				Usage(argv[0]);
			help(argv[0]);
			return 0;
		}
		if (strcmp(argv[index], "--copyright") == 0) {
			if (index != 1)
				Usage(argv[0]);
			copyright();
			return 0;
		}
		if (strncmp(argv[index], "--hash=", 7) == 0) {
			hash = (argv[index]) + 7;
		} else if (strcmp(argv[index], "--quiet") == 0
				|| strcmp(argv[index], "-q") == 0) {
			quiet = true;
		} else if (strcmp(argv[index], "--file") == 0
				|| strcmp(argv[index], "--f") == 0) {
			index++;
			if (index != argc - 1)
				Usage(argv[0]);

			return validate(argv[index]) ? 0 : 1;
		} else if (strncmp(argv[index], "-", 1) == 0
				|| strncmp(argv[index], "--", 1) == 0) {
			Usage(argv[0]);
		} else {
			if (!hash) {
				accumulated = validate(argv[index]);
			}
		}
	}
	return accumulated ? 0 : 1;
}
