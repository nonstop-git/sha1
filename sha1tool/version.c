/**
 * @file
 * Program version command.
 * @author Randall S. Becker
 * @copyright Copyright (c) 2017 Nexbridge Inc. All rights reserved. Proprietary and
 * confidential. Disclosure without written permission violates international
 * laws and will be prosecuted.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>
#include <stdbool.h>
#include <wchar.h>
#include "version.h"

#if ! defined (VERSION)
/**
 * The version variable. This is supplied at compile-time and contains the latest annotated tag,
 * the number of commits from the tag, and the appreviated git commit id where the build occurred.
 */
#define VERSION "unknown"
#endif

/**
 * Usage procedure.
 * @param argv
 *     the set of program arguments without the initial program name.
 */
static void usage(char **argv) {
	wprintf(L"usage: %s --version\n", argv[0]);
}

/**
 * Usage procedure. Calling this method terminates the program with a result code 3.
 * @param argv
 *     the set of program arguments without the initial program name.
 */
static void Usage(char **argv) {
	usage(argv);
	exit(3);
}

/**
 * Dump the copyright notice for this product.
 */
void copyright(void) {
	wprintf(
			L"Copyright (c) 2017 Nexbridge Inc. All Rights Reserved. This program is made\n"
					L"available on an AS-IS basis. No warranty is expressed or implied. Parts of\n"
					L"the code is derived from Git and is released under the GNU General\n"
					L"Public License version 2.0 (http://opensource.org/licenses/GPL-2.0).\n");
}

int version(int argc, char **argv) {
	if (argc != 2 && strcmp(argv[1], "--version") != 0)
		Usage(argv);

	wprintf(L"sha1tool %s\n", VERSION);
	return 0;
}
