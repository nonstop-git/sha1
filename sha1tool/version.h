#pragma once
/**
 * @file
 * sha1tool version command.
 * @author Randall S. Becker
 * @copyright Copyright (c) 2017 Nexbridge Inc. All rights reserved. Proprietary and
 * confidential. Disclosure without written permission violates international
 * laws and will be prosecuted.
 */

/**
 * Check the git version of a Guardian sub-volume with a git working directory.
 * @param argc
 *     the number of command arguments, including the command itself.
 * @param argv
 *     the command arguments.
 * @return the completion code of the command to go to the shell/TACL.
 */
extern int version(int argc, char **argv);

/**
 * Report the application copyright.
 */
extern void copyright(void);
